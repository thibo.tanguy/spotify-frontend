import React from 'react';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import ArtistDetail from './views/ArtistDetail/ArtistDetail.tsx';
import './App.css';
import Header from "./components/Header/header.tsx";
import Preview from "./components/Preview/Preview.tsx";
import Home from "./views/Home/Home.tsx";

const App: React.FC = () => {
    return (
        <>
            <header>
                <Header/>
            </header>

            <main>
                <Router>
                    <Routes>
                        <Route path="/" element={<Home/>}/>
                        <Route path="/artist/:id" element={<ArtistDetail/>}/>
                    </Routes>
                </Router>
            </main>

            <footer>
                <Preview/>
            </footer>
        </>
    );
};

export default App;
