// components/AlbumList.tsx
import React from 'react';
import {Album} from '../../models/Album.tsx';
import TrackList from "../TrackList/TrackList.tsx";
import './AlbumList.css';

interface AlbumListProps {
    albums: Album[];
}

const AlbumList: React.FC<AlbumListProps> = ({albums}) => {
    return (
        <div>
            {albums.map((album) => (
                <div key={album.id}>
                    <h4>{album.name}</h4>
                    <img src={album.images[0].url || ""} alt={`${album.name} cover`}/>

                    <h5>Tracks</h5>
                    <TrackList albumId={album.id}/>
                </div>
            ))}
        </div>
    );
};

export default AlbumList;
