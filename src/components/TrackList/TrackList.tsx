// TrackList.tsx
import React, {useEffect, useState} from 'react';
import {BsPause, BsPlay, BsVolumeUp} from 'react-icons/bs';
import SpotifyService from '../../services/SpotifyService';
import {Track} from '../../models/Track';
import './TrackList.css';

interface TrackListProps {
    albumId: string;
}

const TrackList: React.FC<TrackListProps> = ({albumId}) => {
    const [tracks, setTracks] = useState<Track[]>([]);

    useEffect(() => {
        const fetchTracksByAlbum = async () => {
            try {
                const albumTracks = await SpotifyService.getTracksByAlbum(albumId);
                setTracks(albumTracks);
            } catch (error) {
                console.error('Erreur lors de la récupération des pistes de l\'album', error);
            }
        };

        fetchTracksByAlbum();
    }, [albumId]);

    const handlePlay = (previewUrl: string | undefined) => {
        if (previewUrl) {
            // Ajoutez ici votre logique pour jouer l'audio (par exemple, utilisez une bibliothèque audio)
            console.log(`Lecture de la piste: ${previewUrl}`);
        } else {
            console.warn('Aucun extrait audio disponible pour cette piste.');
        }
    };

    return (
        <ul className="track-list">
            {tracks.map((track) => (
                <li key={track.id} className="track-item">
                    <button
                        className="play-button"
                        onClick={() => handlePlay(track.preview_url)}
                        disabled={!track.preview_url}
                    >
                        {track.isPlaying ? <BsPause/> : <BsPlay/>}
                    </button>
                    <div className="track-info">
                        <span className="track-name">{track.name || ""}</span>
                        <span className="track-duration">{track.duration || ""}</span>
                    </div>
                    <div className="track-controls">
                        <BsVolumeUp className="volume-icon"/>
                        <div className="progress-bar"/>
                    </div>
                </li>
            ))}
        </ul>
    );
};

export default TrackList;
