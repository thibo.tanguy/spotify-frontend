import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import SpotifyService from '../../services/SpotifyService.tsx';
import {Artist} from '../../models/Artist.tsx';
import './search.css';

const Search: React.FC = () => {
    const [query, setQuery] = useState<string>('');
    const [artists, setArtists] = useState<Artist[]>([]);

    useEffect(() => {
        const fetch667artists = async () => {
            try {
                const defaultQuery: string = "jul";
                const result: Artist[] = await SpotifyService.searchArtists(defaultQuery);
                setArtists(result);
            } catch (error) {
                console.error('Erreur lors de la récupération des artistes du 667', error);
            }
        };

        fetch667artists();
    }, []); // Ce tableau vide signifie que cela ne s'exécutera qu'une fois au chargement initial

    const searchArtists = async () => {
        try {
            const result = await SpotifyService.searchArtists(query);
            setArtists(result);
        } catch (error) {
            console.error('Erreur lors de la recherche d\'artistes', error);
        }
    };

    return (
        <div className="search-container">
            <input type="text" value={query} onChange={(e) => setQuery(e.target.value)}/>
            <button onClick={searchArtists}>Rechercher</button>

            <ul className="artist-list">
                {artists.map((artist) => (
                    <li key={artist.id} className="artist-item">
                        <Link to={`/artist/${artist.id}`}>
                            <img src={artist.images[0].url || ""} alt={`${artist.name} cover`}/>
                            <span className="artist-name">{artist.name || ""}</span>
                        </Link>
                    </li>
                ))}
            </ul>
        </div>
    );
};

export default Search;
