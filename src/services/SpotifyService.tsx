import {Artist} from '../models/Artist.tsx';
import {Album} from '../models/Album.tsx';
import {Track} from '../models/Track.tsx';

const BASE_URL: string = 'https://api.spotify.com/v1';
const ACCESS_TOKEN: string = '';

const SpotifyService = {
    async searchArtists(query: string): Promise<Artist[]> {
        try {
            const response: Response = await fetch(`${BASE_URL}/search?q=${query}&type=artist`, {
                headers: {
                    Authorization: `Bearer ${ACCESS_TOKEN}`,
                },
            });

            const data = await response.json();

            if (!response.ok) {
                throw new Error(data.error.message || 'Erreur lors de la recherche d\'artistes');
            }

            return data.artists.items;

        } catch (error) {
            console.error('Erreur lors de la recherche d\'artistes', error);
            throw error;
        }
    },

    async getArtistDetails(artistId: string | undefined): Promise<Artist> {
        try {
            const response: Response = await fetch(`${BASE_URL}/artists/${artistId}`, {
                headers: {
                    Authorization: `Bearer ${ACCESS_TOKEN}`,
                },
            });

            const data = await response.json();

            if (!response.ok) {
                throw new Error(data.error.message || 'Erreur lors de la récupération des détails de l\'artiste');
            }

            return data;

        } catch (error) {
            console.error('Erreur lors de la récupération des détails de l\'artiste', error);
            throw error;
        }
    },

    async getAlbumsByArtistId(artistId: string | undefined): Promise<Album[]> {
        try {
            const response: Response = await fetch(`${BASE_URL}/artists/${artistId}/albums`, {
                headers: {
                    Authorization: `Bearer ${ACCESS_TOKEN}`,
                },
            });

            const data = await response.json();

            if (!response.ok) {
                throw new Error(data.error.message || 'Erreur lors de la récupération des albums');
            }

            return data.items;

        } catch (error) {
            console.error('Erreur lors de la récupération des albums', error);
            throw error;
        }
    },

    async getTracksByAlbum(albumId: string): Promise<Track[]> {
        try {
            const response: Response = await fetch(`${BASE_URL}/albums/${albumId}/tracks`, {
                headers: {
                    Authorization: `Bearer ${ACCESS_TOKEN}`,
                },
            });

            const data = await response.json();

            if (!response.ok) {
                throw new Error(data.error.message || 'Erreur lors de la récupération des pistes');
            }

            return data.items;

        } catch (error) {
            console.error('Erreur lors de la récupération des pistes', error);
            throw error;
        }
    },
};

export default SpotifyService;
