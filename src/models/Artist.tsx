import {Image} from "./Image.tsx";

export interface Artist {
    id: string;
    name: string;
    images: Image[];
}
