import {Artist} from "./Artist.tsx";
import {Album} from "./Album.tsx";

export interface Track {
    id: string;
    name: string;
    duration: string;
    preview_url: string;
    isPlaying: boolean;
    album: Album;
    artists: Artist[];
}