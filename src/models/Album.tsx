import {Artist} from "./Artist.tsx";
import {Image} from "./Image.tsx";

export interface Album {
    id: string;
    name: string;
    images: Image[];
    artist: Artist;
}