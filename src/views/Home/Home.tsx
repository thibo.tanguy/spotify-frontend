import React, {useEffect, useState} from "react";
import "./home.css"
import {Album} from "../../models/Album.tsx";
import SpotifyService from "../../services/SpotifyService.tsx";
import AlbumList from "../../components/AlbumList/AlbumList.tsx";

const Home: React.FC = () => {
    const [albums, setAlbums] = useState<Album[]>([]);

    useEffect(() => {
        const fetchAlbumsByArtist = async (): Promise<void> => {
            try {
                const artistAlbums: Album[] = await SpotifyService.getAlbumsByArtistId('5');
                setAlbums(artistAlbums);
            } catch (error) {
                console.error('Erreur lors de la récupération des albums de l\'artiste', error);
            }
        }

        fetchAlbumsByArtist();
    });

    return (
        <div>
            <h3>Albums</h3>
            <AlbumList albums={albums}/>
        </div>
    );
}

export default Home;