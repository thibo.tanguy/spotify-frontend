import React, {useEffect, useState} from 'react';
import {useParams} from 'react-router-dom';
import SpotifyService from '../../services/SpotifyService';
import {Artist} from '../../models/Artist';
import {Album} from '../../models/Album';
import AlbumList from "../../components/AlbumList/AlbumList.tsx";
import './ArtistDetail.css';

const ArtistDetail: React.FC = () => {
    const {id} = useParams();
    const [artist, setArtist] = useState<Artist | null>(null);
    const [albums, setAlbums] = useState<Album[]>([]);

    useEffect(() => {
        const fetchArtistDetails = async (): Promise<void> => {
            try {
                const artistDetails: Artist = await SpotifyService.getArtistDetails(id);
                setArtist(artistDetails);
            } catch (error) {
                console.error('Erreur lors de la récupération des détails de l\'artiste', error);
            }
        };

        const fetchAlbumsByArtist = async (): Promise<void> => {
            try {
                const artistAlbums: Album[] = await SpotifyService.getAlbumsByArtistId(id);
                setAlbums(artistAlbums);
            } catch (error) {
                console.error('Erreur lors de la récupération des albums de l\'artiste', error);
            }
        };

        fetchArtistDetails();
        fetchAlbumsByArtist();
    }, [id]);

    return (
        <div>
            {artist && (
                <div>
                    <h2>{artist.name}</h2>
                    <img src={artist.images[0].url || ""} alt={`${artist.name || ""} cover`}/>

                    <h3>Albums</h3>
                    <AlbumList albums={albums}/>
                </div>
            )}
        </div>
    );
};

export default ArtistDetail;
